extern crate ch10;

use ch10::larg;

#[test]
fn first() {

    let number_list = vec![34, 40, 25, 100, 65];
    let result = larg::largest(&number_list);

    assert_eq!(result, 10);
}

#[test]
fn second() {
    assert_eq!(1+2, 3);
}
